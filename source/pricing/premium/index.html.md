---
layout: markdown_page
title: "Why Premium?"
---

## GitLab Premium

| Premium helps IT organizations scale their DevOps delivery supporting high availability, geographic replication, progressive deployment, advanced configuration, and consistent standards. | ![Canary Deployment](https://docs.gitlab.com/ee/user/project/img/deploy_boards_canary_deployments.png) |

### **Developer Productivity**

| Ensure your development teams are always able to be productive.  Streamline login, avoid downtime, minimize outages and reduce latency between distributed teams.  | ![GitLab Geo](https://docs.gitlab.com/ee/administration/geo/replication/img/geo_overview.png){: .margin-right20 .margin-left20 .image-width50pct }   |

### **Streamline Project Planning**

|  Manage [multiple agile projects (programs)](https://about.gitlab.com/solutions/agile-delivery/) with intuitive and easy to use dashboards and reports to track issues and milestones across multiple projects.  |  ![Assignee Lists](https://docs.gitlab.com/ee/user/project/img/issue_board_assignee_lists.png)  |

### **Deploy with confidence**

| Accelerate software delivery with integrated deployment and release management. | ![Multiple Project Pipeline Graphs](https://docs.gitlab.com/ee/ci/img/multi_project_pipeline_graph.png) |
| Maintain an end to end picture of how your applications are deployed and delivering business value.  | [![Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/img/index_operations_dashboard_with_projects.png)](https://docs.gitlab.com/ee/user/operations_dashboard/img/index_operations_dashboard_with_projects.png) |

### **Manage the Development Process**  

| Simplify compliance with and traceability with enterprise features built into the developer's workflow. | ![Merge Request Reviews](https://about.gitlab.com/images/feature_page/screenshots/batch-comments.png) |



### Specific Premium features

| **Developer Productivity** |  **Value** |
| [Priority Support](https://about.gitlab.com/support/#priority-support) | Minimize outages and downtime with 4 hour response time for regular business support and 24x7 uptime support with a guaranteed 30 minute response time  |
| [High Availability](https://about.gitlab.com/solutions/high-availability/) | Avoid downtime and outages, ensuring developers are able to work at all times.  |
| [Geographic Replication](https://about.gitlab.com/solutions/geo/) |  Reduce latency between distributed teams and increase developer productivity.  |
| [Smart Card Authentication](https://docs.gitlab.com/ee/administration/auth/smartcard.html) | Simplify and streamline logon process to utilize authentication via smartcard   |
| [Technical Account Manager](https://about.gitlab.com/services/technical-account-management/) |  GitLab account leader will help guide, plan and shape the technical deployment and implementation of GitLab, and partner to help you get the best value possible out of your relationship with GitLab. |



| **Streamline project planning**    | **Value** |
| --------- | ------------ |
| [Group Backlog management](https://docs.gitlab.com/ee/user/project/issue_board.html#multiple-issue-boards) | Simplify tracking, scoping and planning future work with group level backlog management on multiple issue boards.   |
| [Group Milestone Boards/Lists](https://docs.gitlab.com/ee/user/project/issue_board.html#multiple-issue-boards) | Visualize  future work to be delivered in future releases/milestones.   |
| [Assignee Boards/Lists](https://docs.gitlab.com/ee/user/project/issue_board.html) |  Streamline assignment of work to team members in a graphical assignment board.   |
| [Group Issue Boards](https://docs.gitlab.com/ee/user/project/issue_board.html#multiple-issue-boards) |  Visually manage programs (groups) with multiple issue boards where work can be dynamically assigned and tracked.  |
| [Service Desk](https://about.gitlab.com/product/service-desk/) | Streamline the flow of your user's requests for new features and capabilities. |



| **Deploy with confidence**    | **Value** |
| --------- | ------------ |
| [Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/index.html#doc-nav) | a holistic view of the overall health of your company's operations.  |
| [Feature Flags](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html) | Rollout code changes and then dynamically enable or disable specific features. |
| [Deploy Boards](https://docs.gitlab.com/ee/user/project/deploy_boards.html) | Visualize and plan deployments to each environment |
| [Incremental Deployments](https://docs.gitlab.com/ee/topics/autodevops/index.html#incremental-rollout-to-production) | Sequential rollout of code changes minimizes risk |
| [Canary Deployments](https://docs.gitlab.com/ee/user/project/deploy_boards.html#canary-deployments) | Limit risks and roll out changes to a minimal set of end users before initiating rollout to the entire population. |
| [Multi Project Pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipeline_graphs.html) | Link CI pipelines from multiple projects to deliver integrated solutions |
| [Maven Repository](https://docs.gitlab.com/ee/user/project/packages/maven_repository.html) | Maintain library of binary versions of different builds. |
| [NPM Registry](https://docs.gitlab.com/ee/user/project/packages/npm_registry.html) | Every project can have its own space to store javascript NPM packages. |
| [Protected Environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html) | Establish controls and limit the access to change specific environments |

| **Auditing and Compliance**    | **Value** |
| --------- | ------------ |
| [Audit Logs](https://docs.gitlab.com/ee/administration/audit_events.html) | Review changes by user and track access |
| [Auditor users](https://docs.gitlab.com/ee/administration/auditor_users.html) | Read-only access to all projects, groups, and other resources on the GitLab instance |
| [Verified Committer](https://docs.gitlab.com/ee/push_rules/push_rules.html#enabling-push-rules) | Ensure only authorized and verified team members are allowed to commit to the project   |
| [Require Signed Commits](https://docs.gitlab.com/ee/push_rules/push_rules.html#enabling-push-rules) | Enforce policy to require signed commits from contributors  |

| **Manage Development**     | **Value** |
| --------- | ------------ |
| [Merge Request Reviews](https://docs.gitlab.com/ee/user/discussions/index.html#merge-request-reviews-premium) | Track and manage code reviews and feedback with built in merge request reviews |
| [Group and File Templates](https://docs.gitlab.com/ee/user/group/#group-file-templates-premium) | Establish consistent and standard practices |
| [Issue Analytics](https://docs.gitlab.com/ee/user/group/issues_analytics/index.html) | Establish consistent and standard practices |


<center><a href="/sales" class="btn cta-btn orange">Contact sales and learn more about GitLab Premium</a></center>
