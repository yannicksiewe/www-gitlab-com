---
layout: handbook-page-toc
title: "Design"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

**Design** content has been moved to the [**Library**](../library/).
