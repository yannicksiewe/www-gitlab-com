---
layout: job_family_page
title: "Financial Analyst"
---

The financial analyst works directly with members of our senior leadership team to analyze and track the business, plan and derive goals, and collaborate effectively with other departments to achieve them. The Financial Analyst is a detail oriented self starter who can balance multiple projects, and is excited by building processes that scale.

## Levels

* Senior Financial Analyst
* Financial Analyst

## Senior Financial Analyst Responsibilities

* Oversee the company’s financial health and drive monthly reporting packages. Provide ongoing communication and collaboration with business partners. Surface the performance of our key financial and operational metrics to our senior leadership team and the Board.
* Ensure costs are properly forecasted, budgeted and monthly results are analyzed against targets.
* Analyze spend, KPIs and financial performance across products, teams and geographies.
* Working within the Finance organization to understand cost drivers and allocation methodology.
* Responsible for ad-hoc reporting and financial analysis requests working closely with the leadership teams.
* Embrace new information and proactively look for ways to better understand our business performance. Utilize new information to help improve visibility and forecasting.

## Senior Financial Analyst Requirements

* Bachelors in Accounting or Finance preferred; MBA strongly preferred
* Minimum 5+ years of relevant experience with financial analysis background
* Past experience managing cost centers and/or departmental budgets is required
* Experience with scenario analysis
* Prior experience in enterprise technology/software/SaaS company preferred
* Advanced skills in Google office suite, specifically Google Sheets
* 5+ years of hands on experience with data warehousing technologies and visualization layers
* Ability to turn large data sets into meaningful insights. Ability to code using SQL is preferred but not required
* Articulate and concise with an ability to directly communicate with VP level and above 

## Senior Financial Analyst Specialties

* Reports directly to the Finance Business Partner to Sales & Marketing
* Collaborate with the Sales and Marketing departments to design and establish scalable financial processes tailored towards the ongoing needs of these go-to-market department
* Ensure proper standards are met for professional services revenue recognition and oversee the smooth execution of this process.
* Produce professional services cost estimates for statements of work and calculate gross margins

## Financial Analyst Responsibilities

* Plan the wholistic financial trajectory of the business. Work with senior leaders across functional groups to instrument the business. Determine key metrics and measure how resources will be deployed to achieve them.
* Oversee the company’s financial health and drive monthly reporting packages. Provide ongoing communication and collaboration with business partners. Surface the performance of our key financial and operational metrics to our senior leadership team and the Board.
* Embrace new information and proactively look for ways to better understand our business performance. Utilize new information to help improve visibility and forecasting.

## Financial Analyst Requirements

* Bachelor in Accounting or Finance preferred; MBA a plus
* 3-5 years of relevant work experience, preferably at a leading investment bank or fast growing SaaS business.
* Significant experience with sophisticated financial modeling. Direct company FP&A experience is a plus.
* Ability to turn large data sets into meaningful insights. Ability to code using SQL is a plus.
* Solid understanding of the key drivers of a SaaS business.
* Proactive thinker looking to build processes that scale.
* Distill and simplify complex financial concepts for all audiences.
* Build trust and effectively collaborate cross functionally.

## Performance Indicators

* [Time to compile variance analysis reporting](/handbook/finance/financial-planning-and-analysis/#time-to-compile-variance-analysis-reporting)
* [Time to complete investor update reporting](/handbook/finance/financial-planning-and-analysis/#time-to-complete-investor-update-reporting)



