---
layout: job_family_page
title: "Data Engineer"
---

## Requirements

* 2+ years hands-on experience deploying production quality code
* Professional experience using Python, Java, or Scala for data processing (Python preferred)
* Knowledge of and experience with data-related Python packages
* Demonstrably deep understanding of SQL and analytical data warehouses (Snowflake preferred)
* Hands-on experience implementing ETL (or ELT) best practices at scale.
* Hands-on experience with data pipeline tools (Airflow, Luigi, Azkaban, dbt)
* Strong data modeling skills and familiarity with the Kimball methodology.
* Experience with Salesforce, Zuora, Zendesk and Marketo as data sources and consuming data from SaaS application APIs.
* Share and work in accordance with [our values](/handbook/values/)
* Constantly improve product quality, security, and performance
* Desire to continually keep up with advancements in data engineering practices
* Catch bugs and style issues in code reviews
* Ship small features independently

- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).

## Responsibilities

* Maintain our data warehouse with timely and quality data
* Build and maintain data pipelines from internal databases and SaaS applications
* Create and maintain architecture and systems documentation 
* Write maintainable, performant code
* Implement the [DataOps](https://en.wikipedia.org/wiki/DataOps) philosophy in everything you do
* Plan and execute system expansion as needed to support the company's growth and analytic needs
* Collaborate with Data Analysts to drive efficiencies for their work
* Collaborate with other functions to ensure data needs are addressed
* This position reports to the Manager, Data

## Senior Data Engineer

The Senior Data Engineer role extends the [Data Engineer](#responsibilities) role.

### Responsibilities

* All requirements of an Intermediate Data Engineer
* Understand and implement data engineering best practices
* Improve, manage, and teach standards for code maintainability and performance in code submitted and reviewed 
* Create smaller merge requests and issues by collaborating with stakeholders to reduce scope and focus on iteration
* Ship medium to large features independently
* Generate architecture recommendations and the ability to implement them
* Great communication: Regularly achieve consensus amongst teams
* Perform technical interviews

## Staff Data Engineer

The Staff Data Engineer role extends the [Senior Data Engineer](#senior-data-engineer) role.

### Responsibilities
* Develop improvements to data quality, security, and performance that have particular impact across your team and others.
* Solve technical problems of the highest scope and complexity for your team.
* Exert significant influence on the overall objectives and long-range goals of your team.
* Define and extend our internal standards for style, maintainability, and best practices for a high-scale data platform.
* Drive innovation on the team with a willingness to experiment and to boldly confront problems of immense complexity and scope.
* Actively identify and solution impediments and opportunities that impact the velocity and quality of the work done on the entire data team 
* Represent GitLab and its values in public communication around broad initiatives, specific projects, and community contributions. 
* Interact with customers and other external stakeholders as a consultant and spokesperson for the work of your team. 
* Provide mentorship for all Analysts and Engineers on your team to help them grow in their technical responsibilities and remove blockers to their autonomy.
* Confidently ship large features and foundational improvements with minimal guidance and support from other team members or leadership. Collaborate with the team on larger projects.

## Performance Indicators (PI)

*   [SLO achievement per data source](/handbook/business-ops/metrics/#slo-achievement-per-data-source)
*   [Infrastructure Cost vs Plan](/handbook/business-ops/metrics/#infrastructure-cost-vs-plan)
*   [Number of days since last environment audit](/handbook/business-ops/metrics/#number-of-days-since-last-environment-audit)
*   [Mean Time between Failures (MTBF)](/handbook/business-ops/metrics/#mean-time-between-failures-mtbf)
*   [Mean Time to Repair (MTTR)](/handbook/business-ops/metrics/#mean-time-to-repair-mttr)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Manager, Data & Analytics
* Next, candidates will be invited to schedule a second interview with a Data Engineer on the Data & Analytics team
* Next, candidates will be invited to schedule a third interview with members of the Meltano team
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
