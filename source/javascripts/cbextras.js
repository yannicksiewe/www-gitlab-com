!function(){

  var loopcount;
  var attemptedAction;
  var optionalClasses = "";

  var renderMessageStandard = function(loopcount, targets, attemptedAction, optionalClasses)
  {
    var CBNCMessage  = '<div class="cbnc-message' + optionalClasses + '">Trouble ' + attemptedAction + '? You may need to update your <a href="javascript:Cookiebot.renew();">cookie settings</a> to allow <strong>personalization</strong> cookies.</div>';
    var wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'cbneedsconsent');
    targets[loopcount].parentNode.insertBefore(wrapper, targets[loopcount]);
    wrapper.appendChild(targets[loopcount]);
    wrapper.insertAdjacentHTML('afterbegin', CBNCMessage);
  }

  var targets=document.querySelectorAll('iframe[data-src*="youtube"]');
  for(loopcount=0;loopcount<targets.length;loopcount++)
  {
    attemptedAction = "viewing this resource";
    renderMessageStandard(loopcount, targets, attemptedAction, optionalClasses);
  }

  var targets=document.querySelectorAll('iframe[data-src*="calendar"]');
  for(loopcount=0;loopcount<targets.length;loopcount++)
  {
    attemptedAction = "viewing this resource";
    renderMessageStandard(loopcount, targets, attemptedAction, optionalClasses);
  }

  var targets=document.querySelectorAll('#search-handbook');
  for(loopcount=0;loopcount<targets.length;loopcount++)
  {
    attemptedAction = "using search";
    optionalClasses = " cbnc-search-handbook";
    renderMessageStandard(loopcount, targets, attemptedAction, optionalClasses);
  }

}();